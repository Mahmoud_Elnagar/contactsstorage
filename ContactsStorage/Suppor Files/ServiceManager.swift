//
//  ServiceManager.swift
//  UQU
//
//  Created by Mahmoud Elnagar on 4/10/18.
//  Copyright © 2018 uqu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceManager: NSObject
{
    typealias ApiResponse = (Error?, JSON?) -> Void
    
    class func callAPI(url: String, method: HTTPMethod, parameters: [String: Any]?, custumHeaders: [String: String]?, onCompletion: @escaping ApiResponse) -> Void
    {
        print("===============================================")
        print("url = \(url)")
        print("headers = \(custumHeaders ?? [:])")
        print("parameters = \(parameters ?? [:])")
        print("===============================================")
        
        Alamofire.request(URL(string: url)!, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: custumHeaders).responseJSON {response in
            
            print(response)
            if let result = response.result.value
            {
                onCompletion(nil, JSON(result));
            }
            else
            {
                onCompletion(response.result.error, nil);
            }
        }
    }
    
    class func callAPIWithXMLResponse(url: String, method: HTTPMethod, parameters: [String: Any]?, custumHeaders: [String: String]?, onCompletion: @escaping (Error?, String?) -> Void) -> Void {
        
        print("===============================================")
        print("url = \(url)")
        print("headers = \(custumHeaders ?? [:])")
        print("parameters = \(parameters ?? [:])")
        print("===============================================")
        
        Alamofire.request(URL(string: url)!, method: method, parameters: parameters, encoding: URLEncoding.default, headers: custumHeaders).responseString {response in
            
            print(response)
            
            if let result = response.result.value {

                onCompletion(nil, result);
            }else{
                onCompletion(response.result.error, nil);
            }
        }
    }
    
    class func callAPIWithImages(url: String, method: HTTPMethod, parameters: [String: Any]?, custumHeaders: [String: String]?, onCompletion: @escaping ApiResponse) -> Void {
        
        print("===============================================")
        print("url = \(url)")
        print("headers = \(custumHeaders ?? [:])")
        print("parameters = \(parameters ?? [:])")
        print("===============================================")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for key in parameters!.keys {
                
                if let image = parameters![key] as? UIImage
                {
                    if let imageData = image.jpegData(compressionQuality: 0.5)
                    {
                        multipartFormData.append(imageData, withName: key, fileName: "image.png", mimeType: "image/png")
                    }
                    
                }else if let val = parameters![key] as? Int {
                    
                    multipartFormData.append((String(val) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }else {
                    multipartFormData.append((parameters![key] as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, usingThreshold: 6000, to: URL(string: url)!, method: method, headers: custumHeaders) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    if let JSon = response.result.value
                    {
                        print(JSon)
                        onCompletion(nil, JSON(JSon));
                    }
                    else
                    {
                        onCompletion(response.result.error!, nil);
                    }
                }
            case .failure(let encodingError):
                onCompletion(encodingError, nil);
            }
        }
    }
}
