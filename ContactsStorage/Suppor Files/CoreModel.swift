//
//  ContactsListViewController.swift
//  ContactsStorage
//
//  Created by Mahmoud Elnagar on 7/11/19.
//  Copyright © 2019 ME. All rights reserved.
//

import Foundation
import CoreData
import UIKit

enum coreDataTables : String
{
    case contactDataModel = "ContactDataModel"
}

class CoreModel
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context: NSManagedObjectContext!
    init()
    {
        context = appDelegate.persistentContainer.viewContext
    }
    
    func addNewRecord(tableName:String, keyValueData:[String: String])
    {
        let entity = NSEntityDescription.entity(forEntityName: tableName, in: context)
        let newRecord = NSManagedObject(entity: entity!, insertInto: context)
        for (key, value) in keyValueData {
            newRecord.setValue(value, forKey: key)
        }
        
        do {
            try context.save()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func getRecords(tableName:String, keyValueCondition:[String: String]? = nil) -> [NSManagedObject]? {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: tableName)
        
        if let conditions =  keyValueCondition {
            var predicates = [NSPredicate]()
            for condition in conditions {
                predicates.append(NSPredicate(format: "\(condition.key) == %@", "\(condition.value)"))
            }
            let predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: predicates)
            fetchRequest.predicate = predicate
        }
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0 {
                return results as? [NSManagedObject]
            }else{
                return nil
            }
        } catch let err as NSError {
            print(err.debugDescription)
            return nil
        }
    }
    
    func deleteRecords(tableName:String, keyValueCondition:[String: String]? = nil) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: tableName)
        
        if let conditions =  keyValueCondition {
            var predicates = [NSPredicate]()
            for condition in conditions {
                predicates.append(NSPredicate(format: "\(condition.key) == %@", "\(condition.value)"))
            }
            let predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: predicates)
            fetchRequest.predicate = predicate
        }
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0 {
                for result in results {
                    context.delete(result as! NSManagedObject)
                }
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func updateRecords(tableName:String, keyValueToUpdate:[String: String] ,keyValueCondition:[String: String]? = nil) -> Bool {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: tableName)
        
        if let conditions =  keyValueCondition {
            var predicates = [NSPredicate]()
            for condition in conditions {
                predicates.append(NSPredicate(format: "\(condition.key) == %@", "\(condition.value)"))
            }
            let predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: predicates)
            fetchRequest.predicate = predicate
        }
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if let res = results, !res.isEmpty {
                for (key,value) in keyValueToUpdate {
                    res[0].setValue(value, forKey: key)
                }
                 try context.save()
                return true
            }else{
                return false
            }
        } catch let err as NSError {
            print(err.debugDescription)
            return false
        }
    }
}
