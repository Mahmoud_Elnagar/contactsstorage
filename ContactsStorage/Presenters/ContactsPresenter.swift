//
//  ContactsPresenter.swift
//  ContactsStorage
//
//  Created by Mahmoud Elnagar on 7/11/19.
//  Copyright © 2019 ME. All rights reserved.
//

import UIKit
import Firebase
import CoreData

protocol ContactsDelegate : class
{
    func getContactsSuccessfully(contacts: [ContactDataModel]) -> Void
    func contactAddedSuccessfully() -> Void
    func showErrorMessage(message: String) -> Void
    func deleteContactSuccessfully() -> Void
    func editContactSuccessfully() -> Void
}

class ContactsPresenter: NSObject
{
    weak var delegate: ContactsDelegate?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let managedObjectContext: NSManagedObjectContext!
    
    override init()
    {
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    
    func getContacts() -> Void
    {
        if isNetworkReachable
        {
            // Network is Reachable
            let collection = Firestore.firestore().collection("SavedContacts")
            collection.addSnapshotListener { (documentSnapshot, error) in
                if let err = error
                {
                    //Error while trying to get contacts so we get offline data if exist
                    print("Error getting documents: \(err)")
                    let condition = ["isAlreadyDeleted": "false"]
                    
                    if let offlineContacts = CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: condition) as? [ContactDataModel], !offlineContacts.isEmpty
                    {
                        self.delegate?.getContactsSuccessfully(contacts: offlineContacts)
                    }
                    else
                    {
                        self.delegate?.showErrorMessage(message: "")
                    }
                }
                else
                {
                    // comparison between offline data and data from firebase server
                    var contacts = [ContactDataModel]()
                    
                    let offlineContacts = (CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: nil) as? [ContactDataModel]) ?? [ContactDataModel]()
                    
                    for document in documentSnapshot!.documents
                    {
                        print("\(document.documentID) => \(document.data())")
                        
                        let userDict = document.data()
                        let name = userDict["name"] ?? ""
                        let phone = userDict["phone"] ?? ""
                        let documentID = document.documentID
                        
                        var dic = [String : String]()
                        dic["documentID"] = documentID
                        dic["name"] = "\(name)"
                        dic["phone"] = "\(phone)"
                        let contact = Contact.init(parametersJson: userDict, documentId: documentID)
                        
                        if offlineContacts.contains(where: { $0.documentID == documentID })
                        {
                            // This contact already exist.
                            if let contactObj = offlineContacts.first(where: { $0.documentID == documentID }), contactObj.isAlreadyDeleted == "false"
                            {
                                // Get Contact and validate that it is not contained in deleted contacts.
                                if contactObj.name == contact.name && contactObj.phone == contact.phone
                                {
                                    // Normal contact already in shown list.
                                    contacts.append(contactObj)
                                }
                                else
                                {
                                    // This contact is updated offline - we update it in server also.
                                    let documentPath = collection.document(documentID)
                                    documentPath.setData([
                                        
                                        "name": contactObj.name ?? "",
                                        "phone": contactObj.phone ?? ""
                                        
                                        ], completion: { err in
                                            if let err = err
                                            {
                                                print("Error adding document: \(err)")
                                            }
                                    })
                                }
                            }
                        }
                        else
                        {
                            // This is a new contact so we add it directly
                            let contactDataModel = contact.createNSManagedObject(managedObjectContext: self.managedObjectContext)
                            contacts.append(contactDataModel)
                        }
                    }
                    // Update UI with the new list.
                    self.delegate?.getContactsSuccessfully(contacts: contacts)
                }
            }
        }
        else
        {
            //No connection so we get offline data if exist
            let condition = ["isAlreadyDeleted" : "false"]
            
            if let offlineContacts = CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: condition) as? [ContactDataModel], !offlineContacts.isEmpty
            {
                self.delegate?.getContactsSuccessfully(contacts: offlineContacts)
            }
            else
            {
                self.delegate?.showErrorMessage(message: "")
            }
        }
    }
    
    
    func addContact(name: String, phone: String) -> Void
    {
        if name.count > 0 && phone.count > 0
        {
            // Add contact to firebase server
            let collection = Firestore.firestore().collection("SavedContacts")
            
            var ref: DocumentReference? = nil
            ref = collection.addDocument(data: [
                "name": name,
                "phone": phone
            ]) { err in
                if let err = err
                {
                    print("Error adding document: \(err)")
                }
                else
                {
                    print("Document added with ID: \(ref!.documentID)")
                    var dic = [String : String]()
                    dic["documentID"] = ref!.documentID
                    dic["name"] = name
                    dic["phone"] = phone
                    let contact = Contact.init(parametersJson: dic, documentId: ref!.documentID)
                    let _ = contact.createNSManagedObject(managedObjectContext: self.managedObjectContext)
                    
                    self.delegate?.contactAddedSuccessfully()
                }
            }
        }
        else
        {
            self.delegate?.showErrorMessage(message: "Cannot add this contact. kindly fill name and phone.")
        }
    }
    
    func deleteContact(contact: ContactDataModel) -> Void
    {
        // Remove contact from server and save it to deleted names
        let condition = ["documentID": "\(contact.documentID ?? "")"]
        if let offlineContacts = CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: condition) as? [ContactDataModel], !offlineContacts.isEmpty
        {
            let offlineContact = offlineContacts[0]
            offlineContact.isAlreadyDeleted = "true"
            
            do
            {
                try managedObjectContext.save()
            }
            catch let err as NSError
            {
                print(err.debugDescription)
            }
            
            self.delegate?.deleteContactSuccessfully()
        }
        
        if isNetworkReachable
        {
            let collection = Firestore.firestore().collection("SavedContacts")
            let documentPath = collection.document(contact.documentID ?? "")
            documentPath.delete()
            self.delegate?.deleteContactSuccessfully()
        }
    }

    func editContact(contact: ContactDataModel, newName: String, newPhone: String) -> Void
    {
        if newName.count > 0 && newPhone.count > 0
        {
            // Edit contact offline and on firebase server if available
            let condition = ["documentID": "\(contact.documentID ?? "")"]
            if let offlineContacts = CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: condition) as? [ContactDataModel], !offlineContacts.isEmpty
            {
                let offlineContact = offlineContacts[0]
                offlineContact.name = newName
                offlineContact.phone = newPhone
                
                do
                {
                    try managedObjectContext.save()
                }
                catch let err as NSError
                {
                    print(err.debugDescription)
                }
                
                self.delegate?.editContactSuccessfully()
            }
            
            if isNetworkReachable
            {
                let collection = Firestore.firestore().collection("SavedContacts")
                let documentPath = collection.document(contact.documentID ?? "")
                documentPath.setData([
                    
                    "name": newName,
                    "phone": newPhone
                    
                    ], completion: { err in
                        if let err = err
                        {
                            print("Error adding document: \(err)")
                        }
                        else
                        {
                            self.delegate?.editContactSuccessfully()
                        }
                })
            }
        }
        else
        {
            self.delegate?.showErrorMessage(message: "Cannot Edit this contact. kindly fill name and phone.")
        }
    }
}
