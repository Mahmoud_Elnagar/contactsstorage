//
//  ParentViewController.swift
//  ContactsStorage
//
//  Created by Mahmoud Elnagar on 7/11/19.
//  Copyright © 2019 ME. All rights reserved.
//

import UIKit
import MBProgressHUD
import UIEmptyState

class ParentViewController: UIViewController
{
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
    
    
    func showAlert(title: String?, message: String?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = UIColor.mainColor
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true)
    }
    
    func handleEmptyStateData(scrollView: UIScrollView)
    {
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
        
        if let tableView = scrollView as? UITableView
        {
            self.reloadEmptyStateForTableView(tableView)
        }
        else if let collectionView = scrollView as? UICollectionView
        {
            self.reloadEmptyStateForCollectionView(collectionView)
        }
    }
    
    func emptyStatebuttonWasTapped(){}
}

extension ParentViewController : UIEmptyStateDataSource, UIEmptyStateDelegate
{
//    var emptyStateImage: UIImage? {
//
//        return UIImage.init(named: "user")
//    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSAttributedString.Key.foregroundColor: UIColor.mainColor, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18.0)]
        
        let text = "There is no available contacts right now."
        
        return NSAttributedString(string: text, attributes: attrs)
    }
    
    func emptyStateViewWillShow(view: UIView)
    {
        guard let emptyView = view as? UIEmptyStateView else { return }
        // Some custom button stuff
        emptyView.button.layer.cornerRadius = 5
        emptyView.button.layer.borderWidth = 1
        emptyView.button.layer.borderColor = UIColor.mainColor.cgColor
        emptyView.button.layer.backgroundColor = UIColor.mainColor.cgColor
    }
    
    func emptyStatebuttonWasTapped(button: UIButton)
    {
        self.emptyStatebuttonWasTapped()
    }
}


extension UIColor
{
    static var mainColor = UIColor(red: 0.0/255, green: 101.0/255, blue: 111.0/255, alpha: 1)
}
