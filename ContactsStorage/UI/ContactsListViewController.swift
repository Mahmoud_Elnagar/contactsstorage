//
//  ContactsListViewController.swift
//  ContactsStorage
//
//  Created by Mahmoud Elnagar on 7/11/19.
//  Copyright © 2019 ME. All rights reserved.
//

import UIKit
import Firebase

class ContactsListViewController: ParentViewController
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgOffline: UIImageView!
    
    var contacts = [ContactDataModel]()
    let presenter = ContactsPresenter()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        presenter.delegate = self
        presenter.getContacts()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkInternetStatus(notification:)), name: Notification.Name("notifyIntetnetStatus"), object: nil)
    }
    
    @IBAction func addNewUserAction(_ sender: UIButton)
    {
        let alertController = UIAlertController(title: "Save Contact", message: "Enter contact data", preferredStyle: .alert)
        alertController.view.tintColor = UIColor.mainColor
        
        alertController.addTextField { (textField) in
            textField.placeholder = "contact name"
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "phone number"
        }
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { _ in
                                        guard let textFieldName = alertController.textFields?.first,
                                            let textName = textFieldName.text
                                            else { return }
                                        guard let textFieldPhone = alertController.textFields?[1],
                                            let textPhone = textFieldPhone.text
                                            else { return }
                                        
                                        self.presenter.addContact(name: textName, phone: textPhone)
                
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true)
    }
    
    @objc func checkInternetStatus(notification: Notification)
    {
        let isConeected = (notification.object as? Bool) ?? false
        
        if isConeected
        {
            self.imgOffline.isHidden = true
            presenter.getContacts()
        }
        else
        {
            self.imgOffline.isHidden = false
        }
    }
}

extension ContactsListViewController: ContactsDelegate
{
    func showErrorMessage(message: String)
    {
        if message.count > 0
        {
            self.showAlert(title: nil, message: message)
        }
        
        self.handleEmptyStateData(scrollView: self.tableView)
    }
    
    func getContactsSuccessfully(contacts: [ContactDataModel])
    {
        self.contacts = contacts
        self.tableView.reloadData()
        self.handleEmptyStateData(scrollView: self.tableView)
    }
    
    func deleteContactSuccessfully()
    {
        presenter.getContacts()
        self.handleEmptyStateData(scrollView: self.tableView)
    }
    
    func editContactSuccessfully()
    {
        presenter.getContacts()
    }
    
    func contactAddedSuccessfully()
    {
        presenter.getContacts()
    }
}

extension ContactsListViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactTableViewCell
        
        let contact = self.contacts[indexPath.row]
        cell.configureCell(contact: contact)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            
            print("Delete")
            
            let contact = self.contacts[indexPath.row]
            self.presenter.deleteContact(contact: contact)
            
            completionHandler(true)
        }
        deleteAction.backgroundColor = .red
        
        let editAction = UIContextualAction(style: .normal, title: "Edit") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            
            print("Edit")
            
            let contact = self.contacts[indexPath.row]
            
            let alertController = UIAlertController(title: "Save Contact", message: "Enter contact data", preferredStyle: .alert)
            alertController.view.tintColor = UIColor.mainColor
            
            alertController.addTextField { (textField) in
                textField.placeholder = "contact name"
                textField.text = contact.name
            }
            
            alertController.addTextField { (textField) in
                textField.placeholder = "phone number"
                textField.text = contact.phone
            }
            
            let saveAction = UIAlertAction(title: "Save",
                                           style: .default) { _ in
                                            guard let textFieldName = alertController.textFields?.first,
                                                let textName = textFieldName.text
                                                else { return }
                                            guard let textFieldPhone = alertController.textFields?[1],
                                                let textPhone = textFieldPhone.text
                                                else { return }
                                            
                                            self.presenter.editContact(contact: contact, newName: textName, newPhone: textPhone)
                                            
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true)
            
            completionHandler(true)
        }
        editAction.backgroundColor = .darkGray
        
        let swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        swipeConfig.performsFirstActionWithFullSwipe = true
        return swipeConfig
    }
}
