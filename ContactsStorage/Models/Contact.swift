//
//  Contact.swift
//  ContactsStorage
//
//  Created by Mahmoud Elnagar on 7/11/19.
//  Copyright © 2019 ME. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class Contact: NSObject
{
    var documentID: String?
    var name: String?
    var phone: String?
    
    init(parametersJson: [String: JSON])
    {
        if let documentID = parametersJson["documentID"]?.string
        {
            self.documentID = documentID
        }
        
        if let name = parametersJson["name"]?.string
        {
            self.name = name
        }
        
        if let phone = parametersJson["phone"]?.string
        {
            self.phone = phone
        }
    }
    
    init(parametersJson: [String: Any], documentId: String)
    {
        self.documentID = documentId
        
        if let name = parametersJson["name"] as? String
        {
            self.name = name
        }
        
        if let phone = parametersJson["phone"] as? String
        {
            self.phone = phone
        }
    }
    
    func createNSManagedObject(managedObjectContext: NSManagedObjectContext) -> ContactDataModel
    {
        var contactDataModel: ContactDataModel!
        let condition = ["documentID" : "\(self.documentID ?? "")"]
        if let results = CoreModel().getRecords(tableName: coreDataTables.contactDataModel.rawValue, keyValueCondition: condition) as? [ContactDataModel], !results.isEmpty
        {
            for item in results
            {
                if results.first == item
                {
                    contactDataModel = item
                }
                else
                {
                    managedObjectContext.delete(item)
                }
            }
        }
        else
        {
            contactDataModel =  (NSEntityDescription.insertNewObject(forEntityName: coreDataTables.contactDataModel.rawValue, into: managedObjectContext) as! ContactDataModel)
        }
        
        contactDataModel.documentID = self.documentID
        contactDataModel.name = self.name
        contactDataModel.phone = self.phone
        
        do
        {
            try managedObjectContext.save()
        }
        catch let err as NSError
        {
            print(err.debugDescription)
        }
        
        return contactDataModel
    }

}

